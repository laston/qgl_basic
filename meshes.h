#ifndef MESHES_H
#define MESHES_H

#include <QGL>
#include <iostream>
#include "utils.h"

struct Mesh{

    //tablica wierzchołków
    vec3f *vertices = NULL;
    //tablica normalnych
    vec3f *normals = NULL;
    //tablica indeksów
    uint *indices = NULL;
    //tablica współrzędnych tekstury
    vec2f *texture_coor = NULL;
    //tablica współrzędnych tekstury kubicznej
    vec3f *cube_texture_coor = NULL;
    //liczba wierzchołków
    int n_verts = 0;
    //liczba indeksów
    int n_inds = 0;
    GLenum primitive_mode = GL_TRIANGLES;

    GLuint vbo_pos;
    GLuint vbo_normals;

    //wylicza normalne
    void generate_normals();

    //mnożenie współrzędnych przez macierz
    void matrixMultiply(GLfloat *matrix);

    void initBuffers();
    void loadVertices();

    void loadCubicCoords();

    void render();
};

/* Funkcję generujące kształt mesha */

//prostokąt
void plane(Mesh *m, int w, int h);

//okrąg
void circle(Mesh *m, float r, int faces);
void circlereverse(Mesh *m, float r, int faces);

//sześcian
void box(Mesh *m, float w, float h, float l);

//walec
void cylinder(Mesh *m, float r1, float r2, float h, int faces);

//kula
void sphere(Mesh *m, float r, int facesW, int facesH);

//
void expanded_plane(Mesh *m, float width, float height);
void expanded_plane(Mesh *m, QImage &image);

#endif // MESHES_H
