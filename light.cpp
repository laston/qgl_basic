#include "light.h"

Light::Light()
{
}

void Light::setPosition(float x, float y, float z, float w){
    this->position[0] = x;
    this->position[1] = y;
    this->position[2] = z;
    this->position[3] = w;
}

void Light::setAmbient(float red, float green, float blue, float alpha){
    this->ambient[0] = red;
    this->ambient[1] = green;
    this->ambient[2] = blue;
    this->ambient[3] = alpha;
}


void Light::setDiffuse(float red, float green, float blue, float alpha){
    this->diffuse[0] = red;
    this->diffuse[1] = green;
    this->diffuse[2] = blue;
    this->diffuse[3] = alpha;
}

void Light::setSpecular(float red, float green, float blue, float alpha){
    this->specular[0] = red;
    this->specular[1] = green;
    this->specular[2] = blue;
    this->specular[3] = alpha;
}

float* Light::getPosition(){
    return this->position;
}

float* Light::getAmbient(){
    return this->ambient;
}

float* Light::getDiffuse(){
    return this->diffuse;
}

float* Light::getSpecular(){
    return this->specular;
}

