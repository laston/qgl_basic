SOURCES += \
    main.cpp \
    glwidget.cpp \
    utils.cpp \
    matrices.cpp \
    meshes.cpp \
    frames.cpp \
    material.cpp \
    light.cpp \
    texture2d.cpp \
    texturecube.cpp

HEADERS += \
    glwidget.h \
    utils.h \
    matrices.h \
    meshes.h \
    frames.h \
    material.h \
    light.h \
    texture2d.h \
    texturecube.h

QT += opengl widgets

CONFIG += debug

win32{
    LIBS += -lglu32
}

unix{
    LIBS += -lGLU
}

RESOURCES += \
    resources.qrc
