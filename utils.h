#ifndef UTILS_H
#define UTILS_H

#include <QGL>
#include <qmath.h>

struct vec2f{
    float x,y;
};

struct vec3f{
    float x,y,z;
};

struct vec4f{
    float x,y,z,w;
};

struct color3f{
    float r,g,b;
};

struct color4f{
    GLfloat r,g,b,a;
};

/* FUNKCJE UŻYTKOWE */

// przekształcenie stopni na radiany
float deg_to_rad(float deg);

//dodawanie wektorów
vec3f addVectors(vec3f a, vec3f b);
//odejmowanie wektorów (a-b)
vec3f subVectors(vec3f a, vec3f b);
// iloczyn wektorowy
vec3f crossProduct(vec3f a, vec3f b);
//normalizacja wektora
vec3f vecNormalize(vec3f v);


#endif // UTILS_H
