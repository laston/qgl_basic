#include "utils.h"

// przekształcenie stopni na radiany
float deg_to_rad(float deg){
    return deg*M_PI/180;
}

//dodawanie wektorów (a-b)
vec3f addVectors(vec3f a, vec3f b){
    vec3f c = {a.x + b.x, a.y + b.y, a.z + b.z};
    return c;
}

//odejmowanie wektorów (a-b)
vec3f subVectors(vec3f a, vec3f b){
    vec3f c = {a.x - b.x, a.y - b.y, a.z - b.z};
    return c;
}

// iloczyn wektorowy
vec3f crossProduct(vec3f a, vec3f b){
    vec3f v;

    v.x = a.y * b.z - a.z * b.y;
    v.y = a.z * b.x - a.x * b.z;
    v.z = a.x * b.y - a.y * b.x;

    return v;
}

//normalizacja wektora
vec3f vecNormalize(vec3f v){
    float len = sqrtf(v.x * v.x + v.y * v.y + v.z * v.z);
    v.x = v.x / len;
    v.y = v.y / len;
    v.z = v.z / len;

    return v;
}

