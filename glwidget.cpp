#include "glwidget.h"
#include <QMouseEvent>
#include <QKeyEvent>
#include <QDebug>
#include <qmath.h>
#include <QString>
#include <QFileDialog>
#include <iostream>

#define PRINT_GL_ERROR(msg)  { GLenum err; while( (err=glGetError())!= GL_NO_ERROR ) \
{ qDebug() << msg << "in line" << __LINE__ << ": "<< (const char*)gluErrorString(err); } }

GLWidget::GLWidget(QGLWidget *parent) :
    QGLWidget(parent)
{
    connect(&timer, SIGNAL(timeout()), this, SLOT(redraw()));     // timer bedzie wywolywal cyklicznie metode redraw()
    fps = 1;

    lastCursorHPos = 0;
    lastCursorVPos = 0;

    slonceAxisR = 0;
    ziemiaOrbitR = 0;
    ziemiaAxisR = 0;
    ksiezycOrbitR = 0;
    ksiezycAxisR = 0;

    speed = 1;

    m = new Mesh;
    faces = 20;
    //    cylinder(m, 5, 5, 5, faces);
    //    plane(m, 10, 10);
    //        circle(m, 5, faces);
    box(m, 3, 3, 3);
    //    sphere(m, 3, 35,50);
    m->generate_normals();

    skybox = new Mesh;
    box(skybox, 200, 200, 200);
    skybox->generate_normals();
    skybox->loadCubicCoords();

    light_source = new Mesh;
    sphere(light_source, 2, 15,15);
    light_source->generate_normals();

    terrain = new Mesh;

    ziemia = new Mesh;
    slonce = new Mesh;
    ksiezyc = new Mesh;
    sphere(slonce, 5, 50, 50);
    sphere(ziemia, 2, 50, 50);
    sphere(ksiezyc, 0.5, 50, 50);

    slonce->generate_normals();
    ziemia->generate_normals();
    ksiezyc->generate_normals();

    translateMatrixZ.translateMatrix3f(30, 0, 0);
    rotateMatrixOrbitZ.rotateyMatrix3f(0);
    rotateMatrixTiltZ.rotatezMatrix3f(deg_to_rad(-23));

    translateMatrixK.translateMatrix3f(7, 0, 0);
    rotateMatrixOrbitK.rotateyMatrix3f(0);

    light0.setAmbient(0.1f, 0.1f, 0.1f);
    light0.setDiffuse(1.0f, 1.0f, 1.0f);
    light0.setSpecular(1.0f, 1.0f, 1.0f);
    light0.setPosition(51.2f, 100.0f, 20.0f);

    light_source = new Mesh;
    sphere(light_source, 2, 15,15);
    light_source->generate_normals();

    lightframe.pos = {0, 0, 20.0f};
    lightframe.up = {0, 1, 0};
    lightframe.forward = {0, 0, -1};

    lightangle = 0;

    material0.setAmbientColor(0.3f, 0.0f, 0.0f, 1.0f);
    material0.setDiffuseColor(1.0f, 1.0f, 1.0f, 1.0f);
    material0.setSpecularColor(0.1f, 0.1f, 0.1f, 1.0f);
    material0.setShininess(10);

    material1.setAmbientColor(0.5f, 0.4f, 0.1f, 1.0f);
    material1.setDiffuseColor(0.98f, 0.94f, 0.69f, 1.0f);
    material1.setSpecularColor(0.1f, 0.1f, 0.1f, 1.0f);
    material1.setShininess(10);
    material1.setFace(GL_FRONT_AND_BACK);

    active_scene = OBJECT;
    linesActive = FALSE;

    //    setMouseTracking(true);
    std::cout << "init done" << std::endl;

}


void GLWidget::initializeGL()
{
    glEnable(GL_DEPTH_TEST);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glClearColor(0.0f, 0.0f, 0.1f, 0.0f);

    loadSkyboxTexture();

    camera.pos = {0,0,0};
    camera.up = {0,1,0};
    camera.forward = {0,0,-1};

    time.start();                  // start pomiaru czasu
    timer.start(1000.0f / 60.0f);  // start timera wyzwalanego 50 razy na sekunde

}

void GLWidget::resizeGL(int w, int h)
{
    glViewport(0,0, w,h);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60, w/(GLfloat)h, 0.1f, 1000.0f);

    glMatrixMode(GL_MODELVIEW);
    qDebug() << this->width();
}

void GLWidget::redraw()
{
    updateGL();  // updateGL() wywoluje posrednio paintGL()

    // pomiar fps co 100 klatek
    if( (fps % 100) == 0)
    {
        setWindowTitle("FPS: " + QString::number(100 /*klatek*/ * 1000 /*milisekund*/ / time.elapsed()));
        time.restart();
    }
    ++fps;
}

void GLWidget::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();

    //ustawienie początkowe kamery
    MVMatrix.identityMatrix3f();
    MVMatrix.multiply(camera.toCameraMatrix().matrix);
    glLoadMatrixf(MVMatrix.loadMatrix());

    if(linesActive) drawLines();


    glEnable(GL_TEXTURE_CUBE_MAP);
    skyboxTexture.bind();
    drawSkybox(skybox);
    glDisable(GL_TEXTURE_CUBE_MAP);

        lightangle = (lightangle + 0.05);
        lightframe.pos.x = 70 * cos(deg_to_rad(lightangle));
        lightframe.pos.y = 70 * sin(deg_to_rad(lightangle));
        light0.setPosition(lightframe.pos.x, lightframe.pos.y, lightframe.pos.z);


        glEnable(GL_LIGHTING);
        glEnable(GL_LIGHT0);
        setLight(GL_LIGHT0, light0);
    //    glCullFace(GL_FRONT_AND_BACK);

    //    glEnable(GL_FOG);
        glFogi (GL_FOG_MODE, GL_LINEAR);
        GLfloat fogColor[4]= {0.5f, 0.4f, 0.5f, 0.1f};
        glFogfv (GL_FOG_COLOR, fogColor);
        glFogf(GL_FOG_START, 10.0f);
        glFogf(GL_FOG_END, 52.0f);



        //wybór sceny
        switch (active_scene) {
        case OBJECT:
            setMaterial(material1);
            drawMesh(m);
            break;
        case SOLAR_SYSTEM:
            glDisable(GL_LIGHTING);
            drawSolarSystem();
            break;
        case TERRAIN:
            glEnable(GL_TEXTURE_2D);
            ttexture.bind();
    //        drawSkybox(skybox);
            drawTerrain();
            glDisable(GL_TEXTURE_2D);


            MVMatrix.multiply(lightframe.toMatrix().matrix);
            glLoadMatrixf(MVMatrix.loadMatrix());
            glDisable(GL_FOG);
            glDisable(GL_LIGHTING);
            setMaterial(material1);
            drawMesh(light_source);
            break;
        default:
            break;
        }

        glDisable(GL_LIGHTING);

    PRINT_GL_ERROR("ERROR paintGL()");
}

void GLWidget::mousePressEvent(QMouseEvent * e)
{

}

//obsługa myszy
void GLWidget::mouseMoveEvent(QMouseEvent * e)
{
    int h = 0;
    int v = 0;

    if (lastCursorHPos > e->pos().x()) h = 1;
    else if (lastCursorHPos < e->pos().x()) h = -1;

    if (lastCursorVPos > e->pos().y()) v = -1;
    else if (lastCursorVPos < e->pos().y()) v = 1;

    rotateCameraH.rotateMatrix3f(camera.up, deg_to_rad(h));
    camera.mulUp(rotateCameraH.matrix);
    camera.mulForward(rotateCameraH.matrix);

    rotateCameraV.rotateMatrix3f(camera.get_s(), deg_to_rad(v));
    camera.mulUp(rotateCameraV.matrix);
    camera.mulForward(rotateCameraV.matrix);


    lastCursorHPos = e->pos().x();
    lastCursorVPos = e->pos().y();
}

//obsługa klawiatury
void GLWidget::keyPressEvent(QKeyEvent *e)
{
    //ruch kamery w prawo
    if (e->key() == Qt::Key_D){
        vec3f s = crossProduct(camera.up, camera.forward);

        camera.pos.x -= s.x;
        camera.pos.y -= s.y;
        camera.pos.z -= s.z;
    }

    //ruch kamery w lewo
    if (e->key() == Qt::Key_A){
        vec3f s = crossProduct(camera.up, camera.forward);

        camera.pos.x += s.x;
        camera.pos.y += s.y;
        camera.pos.z += s.z;
    }

    //ruch kamery w przód
    if (e->key() == Qt::Key_W){
        camera.pos.x += camera.forward.x;
        camera.pos.y += camera.forward.y;
        camera.pos.z += camera.forward.z;
    }

    //ruch kamery w tył
    if (e->key() == Qt::Key_S){
        camera.pos.x -= camera.forward.x;
        camera.pos.y -= camera.forward.y;
        camera.pos.z -= camera.forward.z;
    }

    //ruch kamery do góry
    if (e->key() == Qt::Key_Q){
        camera.pos.x += camera.up.x;
        camera.pos.y += camera.up.y;
        camera.pos.z += camera.up.z;
    }

    //ruch kamery do dołu
    if (e->key() == Qt::Key_Z){
        camera.pos.x -= camera.up.x;
        camera.pos.y -= camera.up.y;
        camera.pos.z -= camera.up.z;
    }

    //wyjście z programu
    if(e->key() == Qt::Key_Escape)
        close();

    //wyświetlanie elementów sceny z wypełnieniem
    if(e->key() == Qt::Key_1)
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    //wyświetlenie siatki elementów na scenie
    if(e->key() == Qt::Key_2)
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    //wyświetlenie samych wierzchołków elementów na scenie
    if(e->key() == Qt::Key_3)
        glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);

    //prędkość ruchu planet
    if(e->key() == Qt::Key_O){
        if (speed > 0)
            speed -= 1;
    }

    //prędkość ruchu planet
    if(e->key() == Qt::Key_P){
        speed += 1;
    }

    //reset pozycji kamery
    if(e->key() == Qt::Key_R){
        camera.pos = {0,0,0};
        camera.up = {0,1,0};
        camera.forward = {0,0,-1};
    }

    //wyśietlenie osi układu wspórzędnych
    if(e->key() == Qt::Key_F12){
        if(linesActive) linesActive = FALSE;
        else linesActive = TRUE;
    }

    //wyświetlenie na scenie mesha
    if(e->key() == Qt::Key_F1){
        active_scene = OBJECT;
    }

    //wyświetlenie na scenie układu planet
    if(e->key() == Qt::Key_F2){
        active_scene = SOLAR_SYSTEM;
    }

    //wyśietlenie na scenie wygenerowanego terenu
    if(e->key() == Qt::Key_F3){
        active_scene = TERRAIN;

        QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),QString(), tr("Images (*.png *.bmp *.jpg)"));
        std::cout << fileName.toStdString() << std::endl;
        QImage image = QImage(fileName);
        fileName = tr(":/resources/textures/seamless_texture_rock_and_moss_by_koncaliev-d4hhyt5.jpg");
        QImage texture_file = QImage(fileName);
        ttexture = Texture2D(texture_file);
        expanded_plane(terrain, image);
        terrain->generate_normals();
    }
}

//aktywacja wybranego materiału
void GLWidget::setMaterial(Material &material){
    glMaterialfv(material.getFace(), GL_AMBIENT, material.getAmbientColor());
    glMaterialfv(material.getFace(), GL_DIFFUSE, material.getDiffuseColor());
    glMaterialfv(material.getFace(), GL_SPECULAR, material.getSpecularColor());
    glMaterialf(material.getFace(), GL_SHININESS, material.getShininess());
}

//aktywacja wybranego światła
void GLWidget::setLight(GLenum cap, Light &light){
    glLightfv(cap, GL_AMBIENT,  light.getAmbient());
    glLightfv(cap, GL_DIFFUSE,  light.getDiffuse());
    glLightfv(cap, GL_SPECULAR, light.getSpecular());
    glLightfv(cap, GL_POSITION, light.getPosition());
}

//rysuje linie w środku układu współrzędnych
void GLWidget::drawLines(){

    glBegin(GL_LINES);
    {
        glColor3f(1.0f, 0.0f, 0.0f);
        glVertex3f(-1.0f, 0.0f, 0.0f);
        glVertex3f(100.0f, 0.0f, 0.0f);

        glColor3f(0.0f, 1.0f, 0.0f);
        glVertex3f(0.0f, -1.0f, 0.0f);
        glVertex3f(0.0f, 100.0f, 0.0f);

        glColor3f(0.0f, 0.0f, 1.0f);
        glVertex3f(0.0f, 0.0f, -1.0f);
        glVertex3f(0.0f, 0.0f, 100.0f);
    }
    glEnd();
}

void GLWidget::drawMesh(Mesh *mesh){
    glEnable(GL_VERTEX_ARRAY);
    glEnable(GL_NORMAL_ARRAY);
    {
        glVertexPointer(3, GL_FLOAT, 0, mesh->vertices);
        glNormalPointer(GL_FLOAT, 0, mesh->normals);
        glDrawElements(mesh->primitive_mode, mesh->n_inds, GL_UNSIGNED_INT, mesh->indices);
    }
    glDisable(GL_VERTEX_ARRAY);
    glDisable(GL_NORMAL_ARRAY);
}

//rysuje pojedynczego mesha
void GLWidget::drawObject(){
    glEnableClientState(GL_VERTEX_ARRAY);
    {
        glColor3f(1.0f, 0, 1.0f);
        glVertexPointer(3, GL_FLOAT, 0, m->vertices);
        glDrawElements(m->primitive_mode, m->n_inds, GL_UNSIGNED_INT, m->indices);
    }
    glDisableClientState(GL_VERTEX_ARRAY);
}

//rysuje wygenerowany teren
void GLWidget::drawTerrain(){

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    {
        setMaterial(material0);
        glVertexPointer(3, GL_FLOAT, 0, terrain->vertices);
        glNormalPointer(GL_FLOAT, 0, terrain->normals);
        glTexCoordPointer(2, GL_FLOAT, 0, terrain->texture_coor);
        glDrawElements(terrain->primitive_mode, terrain->n_inds, GL_UNSIGNED_INT, terrain->indices);
    }
    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
}

void GLWidget::drawSkybox(Mesh *mesh){
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    {
        glVertexPointer(3, GL_FLOAT, 0, mesh->vertices);
        glNormalPointer(GL_FLOAT, 0, mesh->normals);
        glTexCoordPointer(3, GL_FLOAT, 0, mesh->cube_texture_coor);
        glDrawElements(mesh->primitive_mode, mesh->n_inds, GL_UNSIGNED_INT, mesh->indices);
    }
    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
}

//rysuje układ słoneczny
void GLWidget::drawSolarSystem(){

    glEnableClientState(GL_VERTEX_ARRAY);
    {
        rotateMatrixAxisS.rotateyMatrix3f(deg_to_rad(slonceAxisR = (speed * 0.08 + slonceAxisR)));
        MVMatrix.multiply(rotateMatrixAxisS.loadMatrix());
        glLoadMatrixf(MVMatrix.loadMatrix());

        glColor3f(1, 0.93f, 0);
        glVertexPointer(3, GL_FLOAT, 0, slonce->vertices);
        glDrawElements(slonce->primitive_mode, slonce->n_inds, GL_UNSIGNED_INT, slonce->indices);


        rotateMatrixOrbitZ.rotateyMatrix3f(deg_to_rad(ziemiaOrbitR = (speed * 0.00273 + ziemiaOrbitR)));
        rotateMatrixAxisZ.rotateyMatrix3f(deg_to_rad(ziemiaAxisR = (speed * 20 + ziemiaAxisR)));

        MVMatrix.identityMatrix3f();
        MVMatrix.multiply(camera.toCameraMatrix().matrix);
        MVMatrix.multiply(rotateMatrixOrbitZ.loadMatrix());
        MVMatrix.multiply(translateMatrixZ.loadMatrix());
        rotateMatrixOrbitZ.rotateyMatrix3f(deg_to_rad(-ziemiaOrbitR));
        MVMatrix.multiply(rotateMatrixOrbitZ.loadMatrix());
        MVMatrix.multiply(rotateMatrixTiltZ.loadMatrix());
        MVMatrix.multiply(rotateMatrixAxisZ.loadMatrix());

        glLoadMatrixf(MVMatrix.loadMatrix());

        glColor3f(0.07f, 0.58f, 0.8f);
        glVertexPointer(3, GL_FLOAT, 0, ziemia->vertices);
        glDrawElements(ziemia->primitive_mode, ziemia->n_inds, GL_UNSIGNED_INT, ziemia->indices);

        rotateMatrixOrbitZ.rotateyMatrix3f(deg_to_rad(ziemiaOrbitR));
        rotateMatrixOrbitK.rotateyMatrix3f(deg_to_rad(ksiezycOrbitR = (speed * 0.037 + ksiezycOrbitR)));
        rotateMatrixAxisK.rotateyMatrix3f(deg_to_rad(ksiezycAxisR = (speed * 0.037 + ksiezycAxisR)));

        MVMatrix.identityMatrix3f();
        MVMatrix.multiply(camera.toCameraMatrix().matrix);
        MVMatrix.multiply(rotateMatrixOrbitZ.loadMatrix());
        MVMatrix.multiply(translateMatrixZ.loadMatrix());
        rotateMatrixOrbitZ.rotateyMatrix3f(deg_to_rad(-ziemiaOrbitR));
        MVMatrix.multiply(rotateMatrixOrbitZ.loadMatrix());
        MVMatrix.multiply(rotateMatrixTiltZ.loadMatrix());


        MVMatrix.multiply(rotateMatrixOrbitK.loadMatrix());
        MVMatrix.multiply(translateMatrixK.loadMatrix());
        MVMatrix.multiply(rotateMatrixAxisK.loadMatrix());
        glLoadMatrixf(MVMatrix.loadMatrix());

        glColor3f(0.7f, 0.7f, 0.7f);
        glVertexPointer(3, GL_FLOAT, 0, ksiezyc->vertices);
        glDrawElements(ksiezyc->primitive_mode, ksiezyc->n_inds, GL_UNSIGNED_INT, ksiezyc->indices);
    }
    glDisableClientState(GL_VERTEX_ARRAY);
}


void GLWidget::loadSkyboxTexture(){


    QImage px = QImage("://resources/sky_textures/sky2_xp.png");
    QImage nx = QImage("://resources/sky_textures/sky2_xn.png");
    QImage py = QImage("://resources/sky_textures/sky2_yp.png");
    QImage ny = QImage("://resources/sky_textures/sky2_yn.png");
    QImage pz = QImage("://resources/sky_textures/sky2_zp.png");
    QImage nz = QImage("://resources/sky_textures/sky2_zn.png");


    skyboxTexture = TextureCube(px, nx, py, ny, pz, nz);
}


