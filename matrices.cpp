#include "matrices.h"

void Matrix::identityMatrix3f(){

    for(int i = 0; i < 16; i++){
        matrix[i] = 0;
    }

    matrix[0] = 1;
    matrix[5] = 1;
    matrix[10] = 1;
    matrix[15] = 1;
}

void Matrix::scaleMatrix3f(float sx, float sy, float sz){

    for(int i = 0; i < 16; i++){
        matrix[i] = 0;
    }

    matrix[0] = sx;
    matrix[5] = sy;
    matrix[10] = sz;
    matrix[15] = 1;

}

void Matrix::translateMatrix3f(float tx, float ty, float tz){

    for(int i = 0; i < 16; i++){
        matrix[i] = 0;
    }

    matrix[0] = 1;
    matrix[5] = 1;
    matrix[10] = 1;
    matrix[15] = 1;

    matrix[12] = tx;
    matrix[13] = ty;
    matrix[14] = tz;

}

void Matrix::rotatexMatrix3f(float alpha){
    for(int i = 0; i < 16; i++){
        matrix[i] = 0;
    }

    matrix[0] = 1;
    matrix[5] = cos(alpha);
    matrix[6] = sin(alpha);
    matrix[9] = -sin(alpha);
    matrix[10] = cos(alpha);
    matrix[15] = 1;
}

void Matrix::rotateyMatrix3f(float alpha){

    for(int i = 0; i < 16; i++){
        matrix[i] = 0;
    }

    matrix[0] = cos(alpha);
    matrix[2] = -sin(alpha);
    matrix[5] = 1;
    matrix[8] = sin(alpha);
    matrix[10] = cos(alpha);
    matrix[15] = 1;
}

void Matrix::rotatezMatrix3f(float alpha){

    for(int i = 0; i < 16; i++){
        matrix[i] = 0;
    }

    matrix[0] = cos(alpha);
    matrix[1] = sin(alpha);
    matrix[4] = -sin(alpha);
    matrix[5] = cos(alpha);
    matrix[10] = 1;
    matrix[15] = 1;
}

void Matrix::rotateMatrix3f(vec3f v, float alpha){

    float c = cos(alpha);
    float s = sin(alpha);

    v = vecNormalize(v);

    matrix[0] = v.x * v.x * (1 - c) + c;
    matrix[1] = v.y * v.x * (1 - c) + v.z * s;
    matrix[2] = v.x * v.z * (1 - c) - v.y * s;
    matrix[3] = 0;
    matrix[4] = v.x * v.y * (1 - c) - v.z * s;
    matrix[5] = v.y * v.y * (1 - c) + c;
    matrix[6] = v.y * v.z * (1 - c) + v.x * s;
    matrix[7] = 0;
    matrix[8] = v.x * v.z * (1 - c) + v.y * s;
    matrix[9] = v.y * v.z * (1 - c) - v.x * s;
    matrix[10] = v.z * v.z * (1 - c) + c;
    matrix[11] = 0;
    matrix[12] = 0;
    matrix[13] = 0;
    matrix[14] = 0;
    matrix[15] = 1;
}

void Matrix::multiply(GLfloat *matrix2){

    GLfloat matrix1[16];

    for(int i = 0; i < 16; i++){
        matrix1[i] = matrix[i];
    }


    matrix[0]  = matrix1[0] * matrix2[0]  + matrix1[4] * matrix2[1]  + matrix1[8]  * matrix2[2]  + matrix1[12] * matrix2[3];
    matrix[1]  = matrix1[1] * matrix2[0]  + matrix1[5] * matrix2[1]  + matrix1[9]  * matrix2[2]  + matrix1[13] * matrix2[3];
    matrix[2]  = matrix1[2] * matrix2[0]  + matrix1[6] * matrix2[1]  + matrix1[10] * matrix2[2]  + matrix1[14] * matrix2[3];
    matrix[3]  = matrix1[3] * matrix2[0]  + matrix1[7] * matrix2[1]  + matrix1[11] * matrix2[2]  + matrix1[15] * matrix2[3];
    matrix[4]  = matrix1[0] * matrix2[4]  + matrix1[4] * matrix2[5]  + matrix1[8]  * matrix2[6]  + matrix1[12] * matrix2[7];
    matrix[5]  = matrix1[1] * matrix2[4]  + matrix1[5] * matrix2[5]  + matrix1[9]  * matrix2[6]  + matrix1[13] * matrix2[7];
    matrix[6]  = matrix1[2] * matrix2[4]  + matrix1[6] * matrix2[5]  + matrix1[10] * matrix2[6]  + matrix1[14] * matrix2[7];
    matrix[7]  = matrix1[3] * matrix2[4]  + matrix1[7] * matrix2[5]  + matrix1[11] * matrix2[6]  + matrix1[15] * matrix2[7];
    matrix[8]  = matrix1[0] * matrix2[8]  + matrix1[4] * matrix2[9]  + matrix1[8]  * matrix2[10] + matrix1[12] * matrix2[11];
    matrix[9]  = matrix1[1] * matrix2[8]  + matrix1[5] * matrix2[9]  + matrix1[9]  * matrix2[10] + matrix1[13] * matrix2[11];
    matrix[10] = matrix1[2] * matrix2[8]  + matrix1[6] * matrix2[9]  + matrix1[10] * matrix2[10] + matrix1[14] * matrix2[11];
    matrix[11] = matrix1[3] * matrix2[8]  + matrix1[7] * matrix2[9]  + matrix1[11] * matrix2[10] + matrix1[15] * matrix2[11];
    matrix[12] = matrix1[0] * matrix2[12] + matrix1[4] * matrix2[13] + matrix1[8]  * matrix2[14] + matrix1[12] * matrix2[15];
    matrix[13] = matrix1[1] * matrix2[12] + matrix1[5] * matrix2[13] + matrix1[9]  * matrix2[14] + matrix1[13] * matrix2[15];
    matrix[14] = matrix1[2] * matrix2[12] + matrix1[6] * matrix2[13] + matrix1[10] * matrix2[14] + matrix1[14] * matrix2[15];
    matrix[15] = matrix1[3] * matrix2[12] + matrix1[7] * matrix2[13] + matrix1[11] * matrix2[14] + matrix1[15] * matrix2[15];

}

GLfloat* Matrix::loadMatrix(){
    return matrix;
}
