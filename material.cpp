#include "material.h"

void Material::setAmbientColor(float red, float green, float blue, float alpha){
    this->ambient[0] = red;
    this->ambient[1] = green;
    this->ambient[2] = blue;
    this->ambient[3] = alpha;
}

void Material::setDiffuseColor(float red, float green, float blue, float alpha){
    this->diffuse[0] = red;
    this->diffuse[1] = green;
    this->diffuse[2] = blue;
    this->diffuse[3] = alpha;
}


void Material::setSpecularColor(float red, float green, float blue, float alpha){
    this->specular[0] = red;
    this->specular[1] = green;
    this->specular[2] = blue;
    this->specular[3] = alpha;
}

void Material::setShininess(float shin){
    this->shininess = shin;
}

void Material::setFace(GLenum side){
    this->face = side;
}

float* Material::getAmbientColor(){
    return this->ambient;
}

float* Material::getDiffuseColor(){
    return this->diffuse;
}

float* Material::getSpecularColor(){
    return this->specular;
}

float Material::getShininess(){
    return this->shininess;
}

GLenum Material::getFace(){
    return this->face;
}
