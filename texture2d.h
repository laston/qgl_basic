/* ---------------------------------------------------------------------------
** texture2d.h - 25/3/2015
** Autor: Szymon Dołhan
** -------------------------------------------------------------------------*/

#ifndef TEXTURE2D_H
#define TEXTURE2D_H

#include <QGL>
#include <iostream>

class Texture2D
{
public:
    Texture2D();
    Texture2D(QImage &image);
    void bind();

private:
    GLuint tex;
    int width, height;

};

#endif // TEXTURE2D_H
