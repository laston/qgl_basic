#ifndef FRAMES_H
#define FRAMES_H

#include "utils.h"
#include "matrices.h"

struct Frame{
    vec3f pos;
    vec3f up;
    vec3f forward;

    //mnożenie up przez macierz
    void mulUp(GLfloat* matrix);

    //mnożenie forward przez macierz
    void mulForward(GLfloat* matrix);

    //mnożenie forward przez macierz
    void mulPosition(GLfloat* matrix);

    //obliczanie wektora s
    vec3f get_s();

    //konwersja frame'u do macierzy
    Matrix toMatrix();

    //konwersja frame'u do macierzy kamery
    Matrix toCameraMatrix();
};


#endif // FRAMES_H
