/* ---------------------------------------------------------------------------
** texturecube.cpp - 8/4/2015
** Autor: Szymon Dołhan
** -------------------------------------------------------------------------*/

#include "texturecube.h"

TextureCube::TextureCube()
{
}

TextureCube::TextureCube(QImage &PX, QImage &NX, QImage &PY, QImage &NY, QImage &PZ, QImage &NZ){
    glEnable(GL_TEXTURE_CUBE_MAP);

    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_CUBE_MAP, tex);

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, GL_RGBA, PX.width(), PX.height(), 0, GL_BGRA, GL_UNSIGNED_BYTE, PX.bits());
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, GL_RGBA, PY.width(), PY.height(), 0, GL_BGRA, GL_UNSIGNED_BYTE, PY.bits());
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, GL_RGBA, PZ.width(), PZ.height(), 0, GL_BGRA, GL_UNSIGNED_BYTE, PZ.bits());
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, GL_RGBA, NX.width(), NX.height(), 0, GL_BGRA, GL_UNSIGNED_BYTE, NX.bits());
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, GL_RGBA, NY.width(), NY.height(), 0, GL_BGRA, GL_UNSIGNED_BYTE, NY.bits());
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, GL_RGBA, NZ.width(), NZ.height(), 0, GL_BGRA, GL_UNSIGNED_BYTE, NZ.bits());

    std::cout << "Cube texture loaded" << std::endl;

}

void TextureCube::bind(){
    glBindTexture(GL_TEXTURE_CUBE_MAP, tex);
//    std::cout << "Cube binded" << std::endl;
}
