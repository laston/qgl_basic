/* ---------------------------------------------------------------------------
** texturecube.h - 8/4/2015
** Autor: Szymon Dołhan
** -------------------------------------------------------------------------*/

#ifndef TEXTURECUBE_H
#define TEXTURECUBE_H

#include <QGL>
#include <iostream>

class TextureCube
{
public:
    TextureCube();
    TextureCube(QImage &PX, QImage &NX, QImage &PY, QImage &NY, QImage &PZ, QImage &NZ);
    void bind();

private:
    GLuint tex;

};

#endif // TEXTURECUBE_H
