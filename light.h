#ifndef LIGHT_H
#define LIGHT_H

#include <QGL>

class Light
{
public:
    Light();
    void setPosition(float x, float y, float z, float w = 1.0f);
    void setAmbient(float red, float green, float blue, float alpha = 1.0f);
    void setDiffuse(float red, float green, float blue, float alpha = 1.0f);
    void setSpecular(float red, float green, float blue, float alpha = 1.0f);


    float* getPosition();
    float* getAmbient();
    float* getDiffuse();
    float* getSpecular();




private:
    float position[4];
    float ambient[4];
    float diffuse[4];
    float specular[4];
};

#endif // LIGHT_H
