/* ---------------------------------------------------------------------------
** texture2d.cpp - 25/3/2015
** Autor: Szymon Dołhan
** -------------------------------------------------------------------------*/

#include "texture2d.h"

Texture2D::Texture2D(){

}

Texture2D::Texture2D(QImage &image)
{
    glEnable(GL_TEXTURE_2D);

    this->width = image.width();
    this->height = image.height();
    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex);
    glTexParameteri(GL_TEXTURE_2D,GL_GENERATE_MIPMAP,GL_TRUE);
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
//    GL_LINEAR);
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
//    GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_BGRA, GL_UNSIGNED_BYTE, image.bits());

    std::cout << "tex init done" << std::endl;

}

void Texture2D::bind(){
    glBindTexture(GL_TEXTURE_2D, tex);
}
