#include "meshes.h"

void Mesh::render(){
    std::cout << "HW!"<<std::endl;
}

//wylicza normalne
void Mesh::generate_normals(){

    int faces = n_inds / 3;

    if(normals == NULL)
        normals = new vec3f[n_verts];

    for(int i = 0; i < n_verts; i++)
        normals[i] = {0, 0, 0};

    int v0, v1, v2;
    vec3f v10, v21;
    vec3f n;

    for (int i = 0; i < faces; i++){
        v0 = indices[i*3  ];
        v1 = indices[i*3+1];
        v2 = indices[i*3+2];

        v10 = subVectors(vertices[v1], vertices[v0]);
        v21 = subVectors(vertices[v2], vertices[v1]);
        n = vecNormalize(crossProduct(v21, v10));

        normals[v0] = addVectors(normals[v0], n);
        normals[v1] = addVectors(normals[v1], n);
        normals[v2] = addVectors(normals[v2], n);
    }

    for (int i = 0; i < n_verts; i++){
        normals[i] = vecNormalize(normals[i]);
    }
}

void plane(Mesh *m, int w, int h){

    m->n_verts = 4;
    m->n_inds = 6;

    m->vertices = new vec3f[m->n_verts];
    m->indices = new uint[m->n_inds];

    m->vertices[0].x = w * -1;
    m->vertices[0].y = 0;
    m->vertices[0].z = h;

    m->vertices[1].x = w * -1;
    m->vertices[1].y = 0;
    m->vertices[1].z = h * -1;

    m->vertices[2].x = w;
    m->vertices[2].y = 0;
    m->vertices[2].z = h;

    m->vertices[3].x = w;
    m->vertices[3].y = 0;
    m->vertices[3].z = h * -1;

    m->indices[0] = 0;
    m->indices[1] = 2;
    m->indices[2] = 1;

    m->indices[3] = 1;
    m->indices[4] = 2;
    m->indices[5] = 3;

}

void circle(Mesh *m, float r, int faces){
    m->n_verts = faces +1;
    m->n_inds = 3* faces;

    int z = 0;

    float alpha_rad = 2*M_PI / (float)faces;

    m->vertices = new vec3f[m->n_verts];
    m->indices = new uint[m->n_inds];

    m->vertices[0].x = 0;
    m->vertices[0].y = 0;
    m->vertices[0].z = z;


    for(int i = 1; i < m->n_verts; i++){
        m->vertices[i].x = r*cos(alpha_rad*i);
        m->vertices[i].y = r*sin(alpha_rad*i);
        m->vertices[i].z = z;
    }


    int in = 0;
    for(int i = 0; i < m->n_inds/3; i++){
        m->indices[in++] = 0;
        m->indices[in++] = i+1;
        m->indices[in++] = i+2;
    }
    m->indices[in-1] = 1;

}

void circlereverse(Mesh *m, float r, int faces){
    m->n_verts = faces +1;
    m->n_inds = 3* faces;

    int z = 0;

    float alpha_rad = 2*M_PI / (float)faces;

    m->vertices = new vec3f[m->n_verts];
    m->indices = new uint[m->n_inds];

    m->vertices[0].x = 0;
    m->vertices[0].y = 0;
    m->vertices[0].z = z;


    for(int i = 1; i < m->n_verts; i++){
        m->vertices[i].z = r*cos(alpha_rad*i);
        m->vertices[i].x = r*sin(alpha_rad*i);
        m->vertices[i].y = z;
    }


    int in = 0;
    for(int i = 0; i < m->n_inds/3; i++){
        m->indices[in++] = 0;
        m->indices[in++] = i+1;
        m->indices[in++] = i+2;
    }
    m->indices[in-1] = 1;

}

void box(Mesh *m, float w, float h, float l){
    m->n_verts = 24;
    m->n_inds = 36;

    w *= 0.5;
    h *= 0.5;
    l *= 0.5;

    if(m->vertices != NULL){
        delete [] m->vertices;
        delete [] m->indices;
    }

    m->vertices = new vec3f[m->n_verts];
    m->indices = new uint[m->n_inds];

    for(int i = 0; i < 3; i++){
        m->vertices[i*8+0] = {-l, -h, w};
        m->vertices[i*8+1] = {-l, h, w};
        m->vertices[i*8+2] = {l, -h, w};
        m->vertices[i*8+3] = {l, h, w};
        m->vertices[i*8+4] = {l, -h, -w};
        m->vertices[i*8+5] = {l, h, -w};
        m->vertices[i*8+6] = {-l, -h, -w};
        m->vertices[i*8+7] = {-l, h, -w};
    }

    m->indices[0]  = 0;
    m->indices[1]  = 2;
    m->indices[2]  = 1;

    m->indices[3]  = 1;
    m->indices[4]  = 2;
    m->indices[5]  = 3;


    m->indices[6]  = 6;
    m->indices[7]  = 7;
    m->indices[8]  = 4;

    m->indices[9]  = 7;
    m->indices[10] = 5;
    m->indices[11] = 4;


    m->indices[12] = 2+8;
    m->indices[13] = 4+8;
    m->indices[14] = 3+8;

    m->indices[15] = 3+8;
    m->indices[16] = 4+8;
    m->indices[17] = 5+8;


    m->indices[18] = 0+8;
    m->indices[19] = 1+8;
    m->indices[20] = 6+8;

    m->indices[21] = 1+8;
    m->indices[22] = 7+8;
    m->indices[23] = 6+8;


    m->indices[24] = 0+16;
    m->indices[25] = 6+16;
    m->indices[26] = 2+16;

    m->indices[27] = 6+16;
    m->indices[28] = 4+16;
    m->indices[29] = 2+16;


    m->indices[30] = 1+16;
    m->indices[31] = 3+16;
    m->indices[32] = 7+16;

    m->indices[33] = 7+16;
    m->indices[34] = 3+16;
    m->indices[35] = 5+16;
}

void cylinder(Mesh *m, float r1, float r2, float h, int faces){
    m->n_verts = faces*2;
    m->n_inds = faces*6;

    m->vertices = new vec3f[m->n_verts];
    m->indices = new uint[m->n_inds];

    float alpha_rad = 2*M_PI / (float)faces;



    for(int i = 0; i < faces; i++){
        m->vertices[i*2].x = r1 * cos(alpha_rad*i);
        m->vertices[i*2].y = 0;
        m->vertices[i*2].z = r1 * sin(alpha_rad*i);

        m->vertices[i*2 + 1].x = r2 * cos(alpha_rad*i);
        m->vertices[i*2 + 1].y = h;
        m->vertices[i*2 + 1].z = r2 * sin(alpha_rad*i);
    }


    int in = 0;
    int p;

    for(int i = 0; i < faces; i++){
        p = i*2;
        m->indices[in++] = p;
        m->indices[in++] = p+3;
        m->indices[in++] = p+1;

        m->indices[in++] = p;
        m->indices[in++] = p+2;
        m->indices[in++] = p+3;
    }
    m->indices[in-2] = 0;
    m->indices[in-1] = 1;
    m->indices[in-5] = 1;

}

void sphere(Mesh *m, float r, int facesW, int facesH){
    m->n_verts = 2 + (facesH - 1) * facesW;
    m->n_inds = (facesW * 2 + (facesH - 2) * facesW * 2) * 3;


    m->vertices = new vec3f[m->n_verts];
    m->indices = new uint[m->n_inds];

    m->vertices[0].x = 0;
    m->vertices[0].y = r;
    m->vertices[0].z = 0;

    float dTeta = M_PI / facesH;

    int in = 0;

    float rad = M_PI_2 - dTeta;

    float y1 = r * sin(rad);
    float alpha = 2*M_PI / (float) facesW;

    float r1 = r * cos(rad);

    m->vertices[1].x = r1 * cos(alpha);
    m->vertices[1].y = y1;
    m->vertices[1].z = r1 * sin(alpha);

    for (int i = 0; i < facesW; i++){
        m->vertices[i+1].x = r1 * cos(alpha*i);
        m->vertices[i+1].y = y1;
        m->vertices[i+1].z = r1 * sin(alpha*i);
    }

    for (int i = 0; i < facesW; i++){
        m->indices[in++] = 0;
        m->indices[in++] = i+1;
        m->indices[in++] = i+2;
    }

    m->indices[in-1] = 1;


    for(int i = 1; i < facesH - 1; i++){
        rad = rad - dTeta;
        y1 = r * sin(rad);
        r1 = r * cos(rad);

        for(int j = 0; j < facesW; j++){
            m->vertices[(i*facesW)+j+1].x = r1 * cos(alpha*j);
            m->vertices[(i*facesW)+j+1].y = y1;
            m->vertices[(i*facesW)+j+1].z = r1 * sin(alpha*j);
        }

        for (int j = 0; j < facesW; j++){
            m->indices[in++] = i*facesW+1+j;
            m->indices[in++] = i*facesW+2+j;
            m->indices[in++] = (i-1)*facesW+1+j;

            m->indices[in++] = (i-1)*facesW+1+j;
            m->indices[in++] = i*facesW+2+j;
            m->indices[in++] = (i-1)*facesW+2+j;
        }
        m->indices[in-1] = (i-1)*facesW+1;
        m->indices[in-2] = i*facesW+1;
        m->indices[in-5] = i*facesW+1;

    }


    m->vertices[m->n_verts-1].x = 0;
    m->vertices[m->n_verts-1].y = -r;
    m->vertices[m->n_verts-1].z = 0;


    for (int i = 0; i < facesW; i++){
        m->indices[in++] = m->n_verts-1;
        m->indices[in++] = (facesH-2)*facesW+2+i;
        m->indices[in++] = (facesH-2)*facesW+1+i;
    }

    m->indices[in-2] = (facesH-2)*facesW+1;

//    m->generate_normals();

}

//mnożenie współrzędnych przez macierz
void Mesh::matrixMultiply(GLfloat *matrix){
    int xp, yp, zp;
    int w = 1;

    for(int i = 0; i < n_verts; i++){
        xp = vertices[i].x * matrix[0] + vertices[i].y * matrix[4] + vertices[i].z * matrix[8] + w * matrix[12];
        yp = vertices[i].x * matrix[1] + vertices[i].y * matrix[5] + vertices[i].z * matrix[9] + w * matrix[13];
        zp = vertices[i].x * matrix[2] + vertices[i].y * matrix[6] + vertices[i].z * matrix[10] + w * matrix[14];

        vertices[i].x = xp;
        vertices[i].y = yp;
        vertices[i].z = zp;
    }

}


void expanded_plane(Mesh *m, float w, float h){
    int faces = (w-1)*(h-1)*2;

    float shift_x = w / 2.0f;
    float shift_z = h / 2.0f;

    m->n_verts = w*h;
    m->n_inds = faces *3;

    if(m->vertices !=NULL){
        delete [] m->vertices;
        delete [] m->indices;
        delete [] m->texture_coor;
    }

    m->vertices = new vec3f[m->n_verts];
    m->indices = new uint[m->n_inds];


}

void expanded_plane(Mesh *m, QImage &image){
    int w = image.width();
    int h = image.height();
    int faces = (w-1)*(h-1)*2;

    float shift_x = image.width()/ 2.0f;
    float shift_z = image.height()/ 2.0f;

    m->n_verts = w*h;
    m->n_inds = faces *3;

    if(m->vertices !=NULL){
        delete [] m->vertices;
        delete [] m->indices;
        delete [] m->texture_coor;
    }

    m->vertices = new vec3f[m->n_verts];
    m->indices = new uint[m->n_inds];

    QRgb* c_arr = (QRgb*) image.bits();
    uint color;

    for(int z = 0; z < h; z++){
        for(int x = 0; x < w; x++){
            color = qRed(c_arr[w*z+x]);
            m->vertices[w*z+x] = {(x-shift_x)/10.0, (color/255.0)*10, (z-shift_z)/10.0};
        }
    }

    int in = 0;
    for(int z = 0; z < (h-1); z++){
        for(int x = 0; x < (w-1); x++){
            m->indices[in++] = (x+w*z);
            m->indices[in++] = (x+w*z) + 1;
            m->indices[in++] = ((x+w*z)+w);

            m->indices[in++] = (x+w*z) + 1;
            m->indices[in++] = ((x+w*z)+w) + 1;
            m->indices[in++] = ((x+w*z)+w);
        }
    }

    int tin = 0;
    m->texture_coor = new vec2f[m->n_verts];
    for(int y = 0; y < h; y++){
        for(int x = 0; x < w; x++){
            m->texture_coor[tin].x = 50*x / (float)(w-1);
            m->texture_coor[tin].y = 50*y / (float)(h-1);
            tin++;
        }
    }
//    for (int i =0; i< m->n_verts; i++)
//        std::cout << m->vertices[i].x << " " << m->vertices[i].y << " " << m->vertices[i].z << std::endl;

}

void Mesh::loadCubicCoords(){
    cube_texture_coor = new vec3f[24];

    cube_texture_coor[0]  = {-1.0f, -1.0f, -1.0f};
    cube_texture_coor[8]  = {-1.0f, -1.0f, -1.0f};
    cube_texture_coor[16] = {-1.0f, -1.0f, -1.0f};

    cube_texture_coor[1]  = {-1.0f, 1.0f, -1.0f};
    cube_texture_coor[9]  = {-1,1,-1};
    cube_texture_coor[17] = {-1,1,-1};

    cube_texture_coor[2]  = {1,-1,-1};
    cube_texture_coor[10] = {1,-1,-1};
    cube_texture_coor[18] = {1,-1,-1};

    cube_texture_coor[3]  = {1,1,-1};
    cube_texture_coor[11] = {1,1,-1};
    cube_texture_coor[19] = {1,1,-1};

    cube_texture_coor[4]  = {1,-1,1};
    cube_texture_coor[12] = {1,-1,1};
    cube_texture_coor[20] = {1,-1,1};

    cube_texture_coor[5]  = {1,1,1};
    cube_texture_coor[13] = {1,1,1};
    cube_texture_coor[21] = {1,1,1};

    cube_texture_coor[6]  = {-1,-1,1};
    cube_texture_coor[14] = {-1,-1,1};
    cube_texture_coor[22] = {-1,-1,1};

    cube_texture_coor[7]  = {-1,1,1};
    cube_texture_coor[15] = {-1,1,1};
    cube_texture_coor[23] = {-1,1,1};
}
